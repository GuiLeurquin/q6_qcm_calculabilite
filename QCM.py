import random
import os
clear = lambda: os.system('cls')

questions = open('Questions.txt', 'r')

cours = 1
C = [1,2,3,4,5,6,7,8,9,10,11,12] # Changer les cours sur lesquels on veut être interrogé ici
Q = []
seekingAnswer = 0

for line in questions:
	words = line.split(' ')

	if words[0] == 'Cours':
		cours = int(words[1].rstrip())
	elif cours in C:
		if words[0] == '*' and not seekingAnswer:
			Q.append([line])
			seekingAnswer = 1
		elif seekingAnswer:
			Q[-1].append(line.strip())
			seekingAnswer = 0

questions.close()

n = len(Q)
good_answers = 0
total_answers = 0

clear()
while (len(Q) > 0):
	randint = random.randint(0,len(Q)-1)
	FullQ = Q[randint]

	correctAnswer = FullQ[1].split(' ')
	print()
	print(FullQ[0])
	answer = 0
	while answer not in ["t","f","?","stop"]:
		answer = input("Your answer (T/F/?/STOP): ")
		answer = answer.lower()
	
	if answer == "?" and correctAnswer[0] == 'NO': # No answer
		total_answers += 1
		good_answers += 1
		print('Correct : ' + FullQ[1])
		Q.pop(randint)
	elif answer == "t" and correctAnswer[0] == 'TRUE':
		total_answers += 1
		good_answers += 1
		print('Correct : ' + FullQ[1])
		Q.pop(randint)
	elif answer == "f" and correctAnswer[0] == 'FALSE':
		total_answers += 1
		good_answers += 1
		print('Correct : ' + FullQ[1])
		Q.pop(randint)
	elif answer == "stop":
		break
	else:
		total_answers += 1
		print('Wrong : ' + FullQ[1])

	print ('Score : ' + str(good_answers) + '/' + str(total_answers))
	print ('Questions left : ' + str(len(Q)))
	print()
	input("Press enter for next question")
	clear()


print ('Score : ' + str(good_answers) + '/' + str(total_answers))